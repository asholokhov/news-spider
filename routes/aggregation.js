var _ = require('underscore');

function levenshtein( str1, str2 ) {
    var dist = 0;
    for (var i = 0; i < Math.min(str1.length, str2.length); i++) {
        if (str1.charAt(i) !== str2.charAt(i)) {
            dist++;
        }
    }
    dist += Math.max(str1.length, str2.length) - Math.min(str1.length, str2.length);

    return dist;
}

function fuzzyMatchNews(needle, dictionary, minDistance, minWordLength) {
    var bestDist = minDistance * 2;
    needle = needle.toLowerCase();

    for (var i = 0; i < dictionary.length; i++) {
        // set lower case
        var word = dictionary[i].toLowerCase();

        // skip short words
        if (word.length <= minWordLength) continue;

        // get levenshtein distance
        var dist = levenshtein(needle, word);

        // set best matching
        if (dist < bestDist) {
            bestDist = dist;
        }
    }

    return (bestDist <= minDistance);
}

exports.getAggregated = function(newsArray, callback) {
    var result = [];
    var uniq = [];

    var group = _.groupBy(newsArray, function(item) { return item.source; });
    var maxKey, maxLen = 0;
    for (var src in group) {
        var k = group[src].length;
        if (k > maxLen) {
            maxLen = k;
            maxKey = src;
        }
    }

    console.log('Start aggregation...');
    var err = null;

    try {
        for (var i = 0; i < group[maxKey].length; i++) {
            var current = group[maxKey][i];
            if (current.isAggregated) continue;

            // set empty list of similarly news
            current.similary = [];

            for (var currGroup in group) {
                // if (currGroup === maxKey) continue;

                for (var j = 0; j < group[currGroup].length; j++) {
                    if (i == j) continue; // skip self
                    if (group[currGroup][j].isAggregated) continue;

                    // check every word of current
                    var needlesWords = current.title.split(' ');
                    var matchCount = 0;
                    for (var z = 0; z < needlesWords.length; z++) {
                        if (needlesWords[z].length < 3) continue; // skip short words
                        if (fuzzyMatchNews(needlesWords[z], group[currGroup][j].title.split(' '), 2, 3)) {
                            matchCount++;
                        }
                    }

                    if (matchCount >= Math.round(needlesWords.length / 4)) {
                        current.similary.push(group[currGroup][j]);
                        current.isAggregated = true;
                        group[currGroup][j].isAggregated = true;
                    } else {
                        uniq.push(group[currGroup][j]);
                    }
                }
            }

            result.push(current);
        }
    } catch (e) {
        err = e;
    }

    console.log('Finish!');

    if ("undefined" !== typeof callback) {
        callback(err, result, uniq);
    }
};