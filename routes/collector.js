var http  = require('http');
var jsdom = require('jsdom');
var _     = require('underscore');

var collectedNews = [];
var processedSources = 0;
var pageResponse;

function runAggregation() {
    if (++processedSources !== spiderRules.length) {
        return;
    }

    console.log('News collected!');

    var aggregator = require('./aggregation');
    aggregator.getAggregated(collectedNews, function(err, results, uniq) {
        if (err) throw err;

        pageResponse.render('newsblock', {
            title: 'Spider',
            data: results,
            uniq: uniq
        });
    });
}

var spiderRules = [
    {
        name: 'RIA',
        url: 'http://ria.ru/lenta/',
        handler: function(errors, window) {
            try {
                var $ = window.$;
                $('.list_item').each(function () {
                    collectedNews.push({
                        date: $(this).find('.list_item_date').text(),
                        title: $(this).find('.list_item_title').text(),
                        announce: $(this).find('.list_item_announce').text(),
                        source: "RIA",
                        isAggregated: false
                    });
                });
                console.log('Collecting RIA finished');
            } catch (e) {
                console.log('Failed parse RIA', e);
            }
            runAggregation();
        }
    },
    {
        name: 'RBC Daily',
        url: 'http://rbcdaily.ru/today',
        handler: function(errors, window) {
            try {
                var $ = window.$;
                $('.info-summary__table tr').each(function () {
                    collectedNews.push({
                        date: $(this).find('.info-summary__date-time').text(),
                        title: $(this).find('.info-summary__header-link').text(),
                        announce: $(this).find('.info-summary__text-link').text(),
                        source: "RBC Daily",
                        isAggregated: false
                    });
                });
                console.log('Collecting RBC finished');
            } catch (e) {
                console.log('Failed parse RBC', e);
            }
            runAggregation();
        }
    },
    {
        name: 'Korrespondent',
        url: 'http://korrespondent.net/all/world/',
        handler: function(errors, window) {
            try {
                var $ = window.$;
                $('.article').each(function () {
                    collectedNews.push({
                        date: $(this).find('.article__date').text(),
                        title: $(this).find('.article__title').text(),
                        announce: $(this).find('.article__text').text(),
                        source: "korrespondent.net",
                        isAggregated: false
                    });
                });
                console.log('Collecting Korrespondent finished');
            } catch (e) {
                console.log('Failed parse Korrespondent.net', e);
            }
            runAggregation();
        }
    }
];

function startCollection() {
    processedSources = 0;
    collectedNews = [];

    for (var i = 0; i < spiderRules.length; i++) {
        var func = _.bind(function() {
            console.log('Start collecting ' + this.rule.name);
            jsdom.env({
                url: this.rule.url,
                scripts: ['http://code.jquery.com/jquery.js'],
                done: this.rule.handler
            });
        }, {rule: spiderRules[i]});

        func();
    }
}

exports.index = function(req, res) {
    pageResponse = res;
    startCollection();
};