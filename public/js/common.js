$(document).ready(function() {

    $('#collect-btn').on('click', function() {
        var btn = $(this);
        btn.button('loading');

        $.ajax({
           url: '/collect',
           type: 'GET'
        }).done(function(data) {
            $('.js-news-block').html(data);
        }).fail(function(xhr, status, err) {
            console.log('Collecting failed with error', err);
            throw err;
        }).always(function() {
            btn.button('reset');
        });
    });

});